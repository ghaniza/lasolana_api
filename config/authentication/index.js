const express = require('express');
const { JWTBuilder } = require('./token/jwt');
const { JWTParser } = require('./token/jwt');

const { checkCredentials } = require('./utils');

const tokenEndpoint = express.Router();

const arrayEquals = (authorities = [], required = []) => {
	let count = 0;
	for (let i = required.length; i--;) {
		if (authorities.includes(required[i])) count++;
	}
	if (count === required.length) return true;

	return false;
};


tokenEndpoint.post('/token', async (req, res) => {

	const  { body } = req;

	if (req.headers['content-type'] !== 'application/x-www-form-urlencoded') {
		

		return res.status(400).send({
			error: 'invalid_content_type',
			error_description: 'content-type not allowed for this route',
		});
	}

	if (Object.entries(body).length === 0 && body.constructor === Object) {
		

		return res.status(400).send({
			error: 'missing_body',
			error_description: 'missing authorization body',
		});
	}

	if (!req.headers.authorization || req.headers.authorization === '') {
		

		return res.status(400).send({
			error: 'missing_client_credentials',
			error_description: 'please, provide valid client_id and client_secret',
		});
	}

	if (!body.grant_type || body.grant_type === '') {
		

		return res.status(400).send({
			error: 'missing_grant_type',
			error_description: 'missing grant_type',
		});
	}

	if (!body.username || body.username === '' ) {
	
		return res.status(400).send({
			error: 'invalid_credentials',
			error_description: 'missing username',
		});
	}

	if (!body.password || body.password === '') {
	
		return res.status(400).send({
			error: 'invalid_credentials',
			error_description: 'missing password',
		});
	}

	checkCredentials(req.headers.authorization)
		.then(() => {

			if (body.grant_type === 'password') {

				const builder = new JWTBuilder(body.username, body.password);

				return builder.getJWT()
					.then(data => {
						res.status(201).send({
							access_token: data.token,
							token_type: 'bearer',
							scope: 'password',
							exp: data.exp,
							iat: data.iat,
						});
					})
					.catch((error) => {
						res.status(403).send(error);
					});
			}
		})
		.catch(error => 
			res.status(error.status).send(error)
		);
});



const authorize = (req, res, next, required = []) => {
	const auth = req.headers.authorization || req.query.access_token;
	let jwt;

	if(!auth || auth === '') {
		
		return next({
			status: 403,
			error: 'missing_token',
			error_description: 'a token of a type bearer is expected',
		});
	}

	if (auth.startsWith('Bearer')) {
		jwt = auth.substring(7);
	} else{
		jwt = req.query.access_token;
	}

	const jtwParser = new JWTParser(jwt);

	if (jtwParser.isExpired || !jtwParser.isValid) {
		
		return next({
			status: 403,
			error: 'invalid_token',
			error_description: 'your token is invalid, please, request a new one',
		});
	}

	const { authorities } = jtwParser.content;

	//TODO: [LS-2] Check only required authorities
	if (!arrayEquals(authorities, required)) {

		return next({
			status: 403,
			error: 'forbidden_content',
			error_description: 'you are not permitted to check this content',
		});
	}

	next();
};

module.exports = {tokenEndpoint, authorize};
