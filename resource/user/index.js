const express = require('express');

const router = express.Router();
const utils = require('../../config/authentication/utils');

const {saveDocument} = require('../utils');

const {User} = require('../../model');
const {friendlyId} = require('../utils');


router.get('/', (req, res, next) => {

	const query = User.find(req.query);

	query.exec()
		.then(foundUsers => {
			const result = foundUsers.map((item) => {
				let filtered = new Object();

				const keys = Object.keys(item.toObject());

				keys.forEach(key => {
					if(key !== 'password') filtered[key] = item[key];
				});

				return filtered;

			});
			res.status(200).send(result);
		})
		.catch(() => {
			return next({status: 500, error: 'failed_to_query', error_description: 'server was not able to query the database'});
		});
});


router.post('/', (req, res, next) => {
	const {body} = req;

	const user = new User(body);

	user.validate()
		.then(() => {

			const update = {
				password: utils.encryptPassword(body.password),
				authorities: ['READ'],
				code: friendlyId(4),
			};

			user.set(update);

			saveDocument(user).then( result =>{
				let filtered = new Object();

				const keys = Object.keys(result.toObject());

				keys.forEach(key => {
					if(key !== 'password') filtered[key] = result[key];
				});

				return res.status(201).send(filtered);

			}).catch(error => next(error));

	
		}).catch (error => {
			const response = {
				status: 400,
				error: 'failed_to_validate',
				error_description: error.message || 'check your request body and try again'
			};
			next(response);
		});

});



router.patch('/:email', (req, res, next) => {
	const {email} = req.params;
	const query = User.find({email});

	query.exec()
		.then(foundUsers => {
			const user = foundUsers.shift();
			user.set(req.body);

			saveDocument(user)
				.then(result => {
					return res.status(201).send(result);
				})
				.catch(error => {
					return next(error);
				});
		})
		.catch(() => next({status: 400, error: 'not_found', error_description: 'user was not found in our database'}));
});

module.exports = router;
