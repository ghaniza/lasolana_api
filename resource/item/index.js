const express = require('express');

const mongoose = require('mongoose');

const Item = require('../../model/itemsModel');
const router = express.Router();

const {connect, saveDocument}  = require('../utils');

mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

router.post('/', (req, res) => {
	connect(res, () => {
		req.body.map(doc => {
			const item = new Item(doc);
			saveDocument(item, res);
		});
		
		res.status(200).send({status:'OK'});
	});
});

router.get('/', (req, res) => {
	connect(res, () => {
		Item.find(req.query, (error, foundItems) => {
			if(error) return res
				.status(400)
				.send({
					error: 'search_fail',
					error_description: 'failed to find'
				});
			if(foundItems && foundItems.length > 0)
				return res.status(200).send(foundItems);
			return res.status(400).send({error: 'not_found', error_description: 'nothing was found according to your parameters'});
		});
	});
});

module.exports = router;
