const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');
const Delivery = require('../../model/deliveryModel');
const {connect,} = require('../utils');

router.get('/', (req, res) => 
	connect(res, () => {
		Delivery.find(req.query, (findError, findAnswer) => {
			if (findError)
				return res.status(500).send({
					error: 'failed_to_fetch',
					error_descri3ption: 'a attempt to fetch by id failed'
				});
			return res.status(200).send(findAnswer);
		});
	}));


router.post('/', (req, res) => 
	connect(res, () => {
		const { body } = req;

		const delivery = new Delivery(body);

		return delivery.validate(validationError => {
			if (validationError) {
				
				return res.status(400).send({
					error: 'validation_error',
					error_description: validationError.message
				});
			}

			return delivery.save((saveError, savedDocument) => {
				if (saveError) {
					
					return res.status(500).send({
						error: 'failed_to_save',
						error_description: 'a attempt to persist failed'
					});
				}
				
				return res.status(201).send(savedDocument);
			});
		});
	}));

router.patch('/:id', (req, res) => {
	mongoose.connect(process.env.MONGO_HOST, async connectionError => {
		if (connectionError)
			return res.status(500).send({
				error: 'failed_to_connect',
				error_description: 'a attempt to connect failed'
			});

		try {
			return Delivery.findById(req.query.id, (findError, findAnswer) => {
				if (findError)
					return res.status(500).send({
						error: 'failed_to_fetch',
						error_description: 'a attempt to fetch by id failed'
					});
				if (!findAnswer)
					return res.status(404).send({
						error: 'delivery_not_found',
						error_description:
							'delivery not found, moved, or invalid, please, check your data'
					});

				findAnswer.set(req.body);

				if (req.body.isDelivered != null && req.body.isDelivered) {
					// pusher.trigger('lasolava_backend', 'deliveryCallback', {
					// 	success: true,
					// 	deliveredAt: new Date(),
					// 	id: findAnswer.deliveryId
					// });
				}

				return findAnswer.save((saveError, savedDocument) => {
					if (saveError) {
						
						return res.status(500).send({
							error: 'failed_to_save',
							error_description: 'a attempt to persist failed'
						});
					}
					
					return res.status(200).send(savedDocument);
				});
			});
		} catch (error) {
			
			return res.status(500).send({
				error: 'internal_eror',
				error_description: 'something went wrong'
			});
		}
	});
});

router.delete('/:id', (req, res) => {
	mongoose.connect(process.env.MONGO_HOST, async connectionError => {
		if (connectionError)
			return res.status(500).send({
				error: 'failed_to_connect',
				error_description: 'a attempt to connect failed'
			});

		try {
			return Delivery.findById(req.query.id, (findError, findAnswer) => {
				if (findError)
					return res.status(500).send({
						error: 'failed_to_fetch',
						error_description: 'a attempt to fetch by id failed'
					});
				if (!findAnswer)
					return res.status(404).send({
						error: 'delivery_not_found',
						error_description:
							'delivery not found, moved, or invalid, please, check your data'
					});

				return findAnswer.remove((removeError, removedDocument) => {
					if (removeError)
						return res.status(500).send({
							error: 'failed_to_remove',
							error_description: 'a attempt to remove failed'
						});
					
					return res.status(200).send(removedDocument);
				});
			});
		} catch (error) {
			
			return res.status(500).send({
				error: 'internal_eror',
				error_description: 'something went wrong'
			});
		}
	});
});

module.exports = router;
