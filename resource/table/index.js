const express = require('express');

const router = express.Router();
const mongoose = require('mongoose');
const { Table} = require('../../model');
const {connect, saveDocument, calculateValues} = require('../utils');

mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);

const status = {
	BUSY: 'busy',
	FREE: 'free',
	RESERVED: 'reserved',
	WAITING_PAYMENT: 'waiting_payment',
};

const tables = [
	{
		id: 0,
		status: status.FREE,
		order: '',
		client: '',
		user: '',
	},
];


router.get('/', (req, res, next) => {
	connect(res, ()=>{
		Table.find(req.query, (findError, foundTables) => {
			if(findError || !foundTables){
				
				next({status: 400,error: 'not_found', error_description: 'no tables were found in databese'});
			} 

			res.status(200).send(foundTables.sort((a, b) => a.number - b.number));
			
		});
	});
});

// router.get('/', (req, res) => {
// 	connect(res, ()=> {
// 		Table.find(req.query, (findError, foundTables) => {
// 			if(findError || !foundTables) return res.status(400).send({error: 'not_found', error_description: 'no tables were found in databese'});

// 			async.map(foundTables, (table, callback) => {
// 				const { orderId } = table;
// 				Order.findOne({orderId}, (findErrorOrder, foundOrder) => {
// 					if(findErrorOrder || !foundOrder) {
// 						
// 						callback(null, null);
// 					}else{
// 						const answer = {
// 							status: table.status,
// 							number: table.number,
// 							costurmerId: table.costurmerId,
// 							userId: table.userId,
// 							order: calculateValues(foundOrder),
// 							createdAt: table.createdAt
// 						};
// 						const r = new Table(answer);
// 						callback(null, r);
// 					}

// 				});
// 			}, (error, results) => {
// 				
// 				if (error) return res.status(500).send({error: 'internal_error', error_description: 'something went bad'});
// 				return res.status(200).send(results.filter(table => table != null));
// 			});

// 		});
// 	});
// });

router.get('/:id', (req, res) => {
	try {
		return res.status(200).send(tables[req.params.id]);
	} catch (err) {
		return res.status(400).send({
			error: 'table_not_found',
			error_description: 'table id is invalid or null',
		});
	}
});

router.post('/', (req, res) => {
	connect(res, () => {
		const table = new Table(req.body);

		saveDocument(table)
			.then(result => {
				res.status(201).send(result);
				

			}).catch(error => {
				res.status(400).send(error);
				

			});
	});
});

router.patch('/:id', (req, res) => {
	mongoose.connect(process.env.MONGO_HOST, (err) => {
		if (err) {
			return res.status(500).send({
				error: 'failed_to_save',
				error_description: 'save file failed, database did not replied',
			});
		}

		if (req.query.status) {
			Table.findOne({ number: req.params.id }, (err, doc) => {
				if (err) {
					return res
						.status(400)
						.send({ error: 'bad_request', error_description: err.message });
				}

				doc.set({ status: req.query.status.toUpperCase() });

				doc.validate((err) => {
					if (err) {
						return res.status(400).send({
							error: 'failed_to_validate',
							error_description: 'validation failed due to inconsistencies in your request',
						});
					}
					return null;
				});

				doc.save((err, ans) => {
					if (err) {
						return res.status(500).send({
							error: 'failed_to_save',
							error_description: 'save file failed, database did not replied',
						});
					}
					return res.status(200).send(ans);
				});
			});
		}
	});
});

router.put('/:id', (req, res) => {
	if (req.params.id) {
		const { id } = req.params;

		const keys = Object.keys(tables[id]);

		for (let i = 0; i < keys; i += 1) {
			if (req.body[keys[i]]) {
				tables[id][keys[i]] = req.body[keys[i]];
			}
		}
		res.status(200).send(tables[req.params.id]);
	}
});


router.put('/:number/flagPayment', (req, res, next) => {
	const {number} = req.params;

	connect(res, () => {
		Table.findOne({number}, (findError, foundTable) => {
			if (findError) next({status: 500, error: 'failed_to_fetch', error_description: 'failed to find this table in database'});

			foundTable.set({status: 'WAITING_PAYMENT'});

			saveDocument(foundTable)
				.then(result => {
					res.status(200).send(calculateValues(result));
					next();
					
				})
				.catch(error => {
					next(error);
					
				});
		});
	});
});

module.exports = router;
