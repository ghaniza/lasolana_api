const express = require('express');
const router = express.Router();
const {Flavor} = require('../../model');
const {connect, saveDocument} = require('../utils');

const error = {
	failed_to_find: {error: 'failed_to_find', error_description: 'a fetch to database was finished unsuccessfully'},
};

router.get('/', (req, res) => {
	const { q } = req.query;

	const query = new RegExp(req.query.q.toUpperCase());

	let queries = {name: query};

	if(q){
		if(/(COM:)/.test(req.query.q.toUpperCase())) queries = {description: new RegExp(req.query.q.toUpperCase().replace('COM:', ''))};
		if(/(SEM:)/.test(req.query.q.toUpperCase())) queries = {description: { $not: new RegExp(req.query.q.toUpperCase().replace('SEM:', '')) }};
	}

	connect(res, () => {
		Flavor.find(query ? queries : {}, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send(error.failed_to_find);
			}
			
			return res.status(200).send(foundData);
		});
	});
});

router.get('/:id', (req, res) => {
	connect(res, () => {
		Flavor.findOne(req.query.id, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send(error.failed_to_find);
			}
			
			return res.status(200).send(foundData);
		});
	});
});

router.post('/', (req, res) => {
	connect(res, ()=> {
		const flavor = new Flavor(req.body);
		saveDocument(flavor, res);
	});
});

router.patch('/:id', (req, res) => {
	connect(res, ()=> {
		Flavor.findOne(req.query.id, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send(error.failed_to_find);
			}
			foundData.set(req.body);
			saveDocument(foundData, res);
		});
	});
});

router.delete('/:id', (req, res) => {
	connect(res, ()=> {
		Flavor.findOne(req.query.id, (findError, foundData) => {
			if (findError) res.status(500).send(error.failed_to_find);

			foundData.remove((removeError, deletedDocument) => {
				if (removeError) {
					
					return res.status(500).send(error.failed_to_find);
				}
				
				return res.status(200).send(deletedDocument);
			});
		});
	});
});

module.exports = router;
