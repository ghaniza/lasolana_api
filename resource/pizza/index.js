const express = require('express');
const router = express.Router();
const {Pizza} = require('../../model');
const {connect, saveDocument} = require('../utils');


router.get('/', (req, res) => {
	connect(res, () => {
		Pizza.find(req.query, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send({error: 'failed_to_find', error_description: 'a fetch to database was finished unsuccessfully'});
			}
			
			return res.status(200).send(foundData);
		});
	});
});

router.get('/:id', (req, res) => {
	connect(res, () => {
		Pizza.findOne(req.query.id, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send({error: 'failed_to_find', error_description: 'a fetch to database was finished unsuccessfully'});
			}
			
			return res.status(200).send(foundData);
		});
	});
});

router.post('/', (req, res) => {
	connect(res, ()=> {
		const pizza = new Pizza(req.body);
		saveDocument(pizza, res);
	});
});

router.patch('/:id', (req, res) => {
	connect(res, ()=> {
		Pizza.findOne(req.query.id, (findError, foundData) => {
			if (findError) {
				
				return res.status(500).send({error: 'failed_to_find', error_description: 'a fetch to database was finished unsuccessfully'});
			}
			foundData.set(req.body);
			saveDocument(foundData, res);
		});
	});
});

router.delete('/:id', (req, res) => {
	connect(res, ()=> {
		Pizza.findOne(req.query.id, (findError, foundData) => {
			if (findError) res.status(500).send({error: 'failed_to_find', error_description: 'a fetch to database was finished unsuccessfully'});

			foundData.remove((removeError, deletedDocument) => {
				if (removeError) {
					
					return res.status(500).send({error: 'failed_to_remove', error_description: 'one attempt to remove this document was finished unsuccessfully'});
				}
				
				return res.status(200).send(deletedDocument);
			});
		});
	});
});

module.exports = router;
