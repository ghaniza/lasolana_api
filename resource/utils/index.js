
const crypto = require('crypto');
const async = require('async');

const saveDocument = (document) => new Promise((resolve, reject) =>{
	document.validate(validationError => {
		if (validationError){
			return reject({ status: 400, error: 'failed_to_validate', error_description: validationError.message });
		}

		document.save((saveError, savedDocument) => {
			if (saveError) {
				if(saveError.code && saveError.code === 11000){

					const response ={
						status: 400,
						error: 'failed_to_validate',
						error_description: 'requested user with this email/username is already in our database'
					};

					return reject(response);

				}

				return reject({ status: 500,  error: 'internal_error', error_description: 'save file was not possible' });
			}
			resolve(savedDocument);
		});
	});
});

const saveAll = (documents = []) => new Promise((resolve, reject) => async.map(documents, (document, error)=> {
	document.validate(validationError => {
		if (validationError){
			error({error: 'failed_to_validate', error_description: validationError.message });
		}

		document.save((saveError, savedDocument) => {
			if (saveError) {
				error({ error: 'internal_error', error_description: 'save file was not possible' });
			}
			return savedDocument;
		});
	});
}, (error, results) => {
	if (error) reject(error);
	else resolve(results);
}));

const friendlyId = lenght => crypto
	.randomBytes(Math.ceil((lenght * 3) / 4))
	.toString('base64')
	.slice(0, lenght)
	.replace(/\+/g, '0')
	.replace(/\//g, '0');


const calculateValues = (doc) => {
	let total = doc.total || 0;
	const result = doc;

	let paid = 0;

	result.items.forEach(item => {
		total += item.subtotal;
	});


	result.payments.forEach(payment => {
		paid += payment.value;
	});

	const serviceTax = total * 0.1; //TODO valor da taxa de serviço
	const final = total + serviceTax;
	const remaining = Math.max(0, final - paid);
	const change = Math.max(0, paid - final);

	result.change = change.toPrecision(6);
	result.serviceTax = serviceTax.toPrecision(6);
	result.total = total.toPrecision(6);
	result.final = final.toPrecision(6);
	result.paid = paid.toPrecision(6);
	result.remaining = remaining.toPrecision(6);

	return result;
};

module.exports = {saveDocument, saveAll, friendlyId, calculateValues};
