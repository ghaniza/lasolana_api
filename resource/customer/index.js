const express = require('express');

const router = express.Router();
const crypto = require('crypto');
const {saveDocument} = require('../utils');

const { Customer } = require('../../model');

router.get('/', (req, res, next) => {
	return Customer.find(req.query, (findError, findAnswer) => {
		if (findError || findAnswer.length === 0)
			return next({
				status: 400,
				error: 'customer_not_found',
				error_description:
							'customer not found, moved, or invalid, please, check your data'
			});
		return res.status(200).send(findAnswer);
	});
});

router.post('/', (req, res, next) => {
	const { body } = req;

	body.customerId = crypto.randomBytes(6).toString('hex');
	body.createdAt = new Date();

	const customer = new Customer(body);

	saveDocument(customer)
		.then(answer => {
			res.status(201).send(answer);
		})
		.catch(error => next(error));
});

router.patch('/:id', (req, res, next) => {
	return Customer.findById(req.params.id, (findError, findAnswer) => {
		if (findError)
			return res.status(500).send({
				error: 'failed_to_fetch',
				error_description: 'a attempt to fetch by id failed'
			});
		if (!findAnswer)
			return res.status(404).send({
				error: 'customer_not_found',
				error_description:
							'customer not found, moved, or invalid, please, check your data'
			});

		findAnswer.set(req.body);

		saveDocument(findAnswer)
			.then(answer => {
				res.status(201).send(answer);
			})
			.catch(error => next(error));
	});
});

module.exports = router;
