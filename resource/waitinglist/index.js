const express = require('express');

const router = express.Router();
const {WaitingList} = require('../../model');

const {connect, saveDocument} = require('../utils');

router.get('/', (req, res, next) => {
	connect(res, () => {
		WaitingList.find(req.query, (findError, foundLine) => {
			if(findError || !foundLine){
				
				next({status: 400, error: 'fetch_error', error_description: 'failed to fetch the database' });
			}

			res.status(200).send(foundLine);
			
			next();
		});
	});
});

router.post('/', (req, res, next) => {
	connect(res, () => {
		const user = new WaitingList(req.body);

		saveDocument(user)
			.then(result => {
				next();
				
				res.status(200).send(result);
			})
			.catch(error => {
				
				next(error);
			});
	});
});

router.delete('/:id', (req, res, next) => {
	connect(res, () => 
		WaitingList.findByIdAndDelete(req.params.id, (findError, deletedDocument) => {
			res.status(200).send(deletedDocument);
			
			next();
		})
	);
});

module.exports = router;