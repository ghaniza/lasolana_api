const express = require('express');
var async = require('async');
var bcrypt = require('bcrypt');

const router = express.Router();
const {Order, Pizza, Flavor, Table, Payment, Delivery} = require('../../model');
const {connect, saveDocument, friendlyId, calculateValues} = require('../utils');
const fetch = require('node-fetch');

router.get('/', (req, res, next) => {
	Order.find(req.query, (findError, foundDocuments) => {
		console.log(findError);
		if(findError){
			return next({status: 404, error: 'order_not_found', error_description: 'no orders were find within your query params'});
		}

		if(!req.query.archive) res.status(200).send(foundDocuments.filter(document => !document.closed));
		else res.status(200).send(foundDocuments);
	});
});

router.post('/', (req, res, next) => {

	const {user, body} = req;

	const orderId = friendlyId(5);

	Table.find({number: body.tableNumber}, (findError, foundTable) => {
		if(findError)
			next({status: 400, error: 'unknown_error', error_description: 'could not get an answer from database'});

		if(foundTable.length === 0){

			const newTable = new Table({
				number: body.tableNumber,
				customer: body.customer,
				status: 'BUSY',
			});

			const order = new Order(body);

			order.set({
				orderId,
				user: `${user.code} - ${user.name}`,
			});

			saveDocument(order)
				.then(orderResult => {
						
					newTable.set({order: orderResult, orderId});

					saveDocument(newTable)
						.then(tableResult => {

							res.status(201).send(tableResult);
							
							next();

						}).catch(error =>{
							
							next(error);
						});

				}).catch(error =>{
					
					next(error);
				});

		} else {
			
			next({status: 400, error: 'invalid_table', error_description: 'requested table is already in use'});
		}
	});

});

router.post('/delivery', (req, res, next) => {

	const {user, body} = req;

	const id = friendlyId(5);

	if(!body.customer || body.customer === '') next({status: 400, error: 'customer_not_found', error_description: 'a customer must be set for deliveries'});

	const deliveryBody = {};

	deliveryBody.paymentMethod = body.paymentMethod;
	deliveryBody.address = body.address;
	deliveryBody.customer = body.customer;
	deliveryBody.orderId = id;

	const delivery = new Delivery(deliveryBody);

	const validationError = delivery.validateSync();

	if(validationError) next({status: 400, error: 'failed_to_validate', error_description: 'please, verify your delivery request body'});

	const order = {};

	order.orderId = id;
	order.user = `${user.code} - ${user.name}`;
		
	formatProducts(body.products)
		.then(result => {
			order.items = result;

			const saveOrder = new Order(order);

			saveDocument(saveOrder)
				.then(orderResult => {
					delivery.set({order: orderResult});

					const access_token ='hXlnoY101I1bbcY78guRDvNT4T6IfEQxgpm-pqNIQN0';
					const geoapi = 'https://geocoder.ls.hereapi.com/6.2/geocode.json';

					const {street, number, district} = delivery.address;

					const querystr = `${street}, ${district}, ${number}, Florianópolis, Santa Catarina, BR`;

					fetch(`${geoapi}?apiKey=${access_token}&searchText=${querystr}`)
						.then(result => {
							if(result.status === 200){
								result.json()
									.then(json => {
										delivery.set({address: {...delivery.address, geo: json.Response.View[0].Result[0].Location.DisplayPosition}});
										saveDocument(delivery)
											.then(result => {
												res.status(200).send(result);
												next();
												
											})
											.catch(error => {
												next(error);
												
											});
									}).catch(error => {
										
										next(error);
									});
							}
						}).catch(error => {
							
							next(error);
						});
				});
		})
		.catch(error =>  next(error));
});




const formatProducts = (products = []) => new Promise((resolve, reject) => {
	let result = [];

	async.map(products, (product, callback) => {

		const {code, quantity, aditionals, owner} = product;

		if(!product || !product.code || !product.quantity)
			return reject({
				code: 400,
				message: {
					error: 'invalid_data',
					error_description: 'your request data is null or invalid, please, check if quantity and code are both okay'
				}
			});


		let value = code.toString();
		let pizza;

		const isPizza = /\*/.test(value);

		if (isPizza){
			pizza = value.substring(0,2);
			const rest = value.substring(3).length;

			const codeResult = [];

			switch(rest){
			case 4:
				codeResult.push(value.substring(3));
				break;
			case 8:
				codeResult.push(value.substring(3, 7));
				codeResult.push(value.substring(7));
				break;
			case 12:
				codeResult.push(value.substring(3, 7));
				codeResult.push(value.substring(7, 11));
				codeResult.push(value.substring(11));
				break;
			default:
				reject({code: 400, message: {error: 'invalid_code', error_description: 'verify your code and try again'}});
				break;
			}

			Flavor.find({code: {$in: codeResult }}, (findError, foundFlavors) => {
				if (findError || foundFlavors.length !== codeResult.length){
					const notFound = codeResult.filter(code => foundFlavors.find(flavor => flavor.code === code) == undefined);
					return reject({code: 400, message: {error: 'file_not_found', error_description: `one of requested flavors(${notFound}) does not exists or it is deleted`}});
				}

				Pizza.findOne({code: pizza}, (findError, foundPizza) => {
					if (findError || !foundPizza)
						return reject({code: 400, message: {error: 'file_not_found', error_description: `the requested pizza(${pizza}) does not exists or it is deleted`}});

					const description = codeResult.map((flavor) => {
						let string = `${flavor} - ${foundFlavors.find(item => item.code === flavor).name}`;

						if(aditionals && Array.isArray(aditionals) && aditionals.length > 0){

							let result = '(';

							if (aditionals.length === 0) return string;

							aditionals.forEach(aditional => {
								const arr = aditional.split(':');

								if(arr[0] == flavor)
									result += `${arr[1]}, `;
							});

							result = result.substring(0, result.length - 2) + ')';

							string = string + ' ' + result;
						}
						return string;
					}
					);


					let price = 0;

					foundFlavors.forEach(flavor => {
						switch(foundPizza.code){
						case '10':
							price += flavor.toObject().small / foundFlavors.length;
							break;
						case '11':
							price += flavor.toObject().medium / foundFlavors.length;
							break;
						case '12':
							price += flavor.toObject().large / foundFlavors.length;
							break;
						default:
							price += flavor.toObject().large / foundFlavors.length;
							break;
						}
					});


					result.push({
						quantity,
						code,
						title: `${pizza} - PIZZA ${foundPizza.name}`,
						description,
						owner: (Array.isArray(owner) ? owner : (owner ? [owner] : ['Geral'])),
						price,
						paid: 0,
						subtotal: quantity * price
					});
					callback(null, result);
				});
			});
		}
	}, () => resolve(result));
});

router.put('/:orderId/:code/remove', (req, res, next) => {
	const {orderId, code} = req.params;
	connect(res, () => {
		Order.findOne({orderId}, (orderFindError, foundOrder) => {

			if(orderFindError || foundOrder == null || foundOrder.closed) {
				
				return res.status(400).send({error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
			}

			if(code && code !== ''){
				let numberAdded = 0;
				let numberRemoved = 0;

				foundOrder.items.forEach(item => {
					if(item.code == code){
						if(item.price >= 0) numberAdded++;
						if(item.price < 0) numberRemoved++;
					}
				});

				if(numberAdded == numberRemoved){
					
					return res.status(400).send({error: 'cannot_remove', error_description: 'there are no products with this code to remove'});
				}

				const product = foundOrder.items.find(item => item.code === code);

				if(product == null || product == undefined){
					
					return res.status(400).send({error: 'invalid_params', error_description: 'code and id were expected as url queries'});
				}

				product.price *= -1;
				product.subtotal *= -1;
				product.title = `${product.title} (REMOVIDO)`;

				Order.findByIdAndUpdate(foundOrder.id, {$push: {items: product}}, {new: true}, (pushError, pushedDocument) => {
					if(pushError) {
						
						return res.status(400).send({error: 'error_at_push', error_description: 'update file failed'});
					}

					const document = calculateValues(pushedDocument);
					
					next();
					return res.status(200).send(document);
				});

			}else {
				
				return res.status(400).send({error: 'invalid_params', error_description: 'code and id were expected as url queries'});
			}
		});
	});
});


router.put('/:orderId/add', (req, res, next) => {
	const {orderId} = req.params;

	Order.findOne({orderId}, async (findError, foundDocument) => {
		if (findError || !foundDocument || foundDocument.closed) {
			
			next({status: 500, error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
			return;

		}
		formatProducts(req.body).then(results => {
			let items = [];

			foundDocument.items.forEach(item => {
				items.push(item);
			});

			items = items.concat(results);

			foundDocument.set({items});

			foundDocument.save((saveError, savedFile) => {
				if (saveError) return res.status(500).send({error: 'failed_to_persist', error_description: 'requested order could not be updated at the moment'});

				const result = calculateValues(savedFile);
				Table.findOne({ orderId }, (findError, foundTable) => {
					if (findError || foundTable == null) {
						
						next({status: 400, error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
						return;
					}

					foundTable.set({
						order: result
					});

					saveDocument(foundTable)
						.then(resultTable => {
							res.status(200).send(resultTable);
							
							next();
						})
						.catch(() => {
							
							next({status: 500, error: 'failed_to_persist', error_description: 'table update could not be saved'});
						});
				});

			});

		}).catch(error => {
			res.status(error.code || 500).send(error.message);
		});

	});
});

router.get('/:orderId', (req, res) => {
	const {orderId} = req.params;
	Order.findOne({orderId}, (findError, foundOrder) => {
		if(findError || foundOrder == null || foundOrder.closed){
			
			return res.status(400).send({error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
		}

		
		res.status(200).send(foundOrder);
	});
});

router.get('/:orderId/members', (req, res) => {
	const {orderId} = req.params;

	Order.findOne({orderId}, (findError, foundOrder) => {
		if(findError || foundOrder == null || foundOrder.closed){
			
			return res.status(400).send({error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
		}

		
		const members = [];

		foundOrder.items.forEach(item => {
			if (members.find(member => member.toUpperCase() === item.owner.toUpperCase()) == undefined)
				members.push(item.owner.toUpperCase());
		});

		members.sort();

		res.status(200).send(members);
	});
});

router.post('/:orderId/pay', (req, res, next) => {
	const body = req.body;

	const {orderId} = req.params;

	const payment = new Payment(body);
	const timestamp = new Date().getTime();

	Order.findOne({orderId}, (findError, foundOrder) => {
		if(findError || foundOrder == null || foundOrder.closed){
			
			return res.status(400).send({error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
		}

		const values = calculateValues(foundOrder);
		if(values.remaining <= 0){
			
			return res.status(400).send({error: 'bill_already_paid', error_description: 'requested order was already paid'});
		}

		if(values.remaining < payment.value && payment.method !== 'DINHEIRO'){
			
			return res.status(400).send({error: 'invalid_payment', error_description: 'changes are not allowed for payments different than cash'});
		}

		const hash = bcrypt.hashSync(foundOrder.user + timestamp + foundOrder.orderId, process.env.SALT);

		payment.set({authorizationDate: timestamp, authorizationId: hash});

		payment.validate()
			.then(() => {
				Order.findOneAndUpdate(
					{
						orderId
					},
					{
						$push: {payments: {member: 'Geral', payment}}
					},
					{
						new: true
					},
					(pushError, newDocument) => {
						if (pushError){
							
							return res.status(400).send({error: 'failed_to_push', error_description: 'couldnt query the database'});
						}

						
						const result = calculateValues(newDocument);
						next();
						return res.status(200).send(result);

					});
			})
			.catch(error => {
				
				res.status(400).send({error: 'failed_to_validate', error_description: `invalid payment format: ${error.message}`});
			});

	});


});
	
router.post('/:orderId/:member/pay/:code', (req, res, next) => {
	const {member, code, orderId} = req.params;
	const {user} = req;

	Order.findOne({orderId}, (findError, foundOrder)=> {
		if(findError || !foundOrder){
			
			next({status: 500, error: 'not_found', error_description: 'requested order was not found or deleted'});
		}

		const item = foundOrder.items.filter(value => (value.code === code && value.owner.includes(member)))[0];

		if(item.paid === item.subtotal){
			
			next({status: 400, error: 'already_paid', error_description: 'requested item was already paid'});
		}


		const issuer = `${user.code} - ${user.name}`;
		const authorizationDate = new Date();
		const authorizationId = bcrypt.hash(foundOrder.user + authorizationDate + foundOrder.orderId, process.env.SALT);
		const payment = new Payment({...req.body, authorizationDate, authorizationId, operator: issuer});

			
		saveDocument(payment).then(result => {
			
			res.status(201).send(result);
			next();
		})
			.catch(error => {
				
				next(error);
			});

	});

});



router.post('/:orderId/:member/pay', (req, res, next) => {
	const body = req.body;

	const {orderId, member} = req.params;

	const payment = new Payment(body);
	const timestamp = new Date().getTime();

	Order.findOne({orderId}, (findError, foundOrder) => {
		if(findError || foundOrder == null || foundOrder.closed){
			
			return res.status(400).send({error: 'file_not_found', error_description: 'requested order does not exists or it is deleted/closed'});
		}

		const items = foundOrder.items.filter(item => item.owner = member);

		const paid = foundOrder.payments.filter(oldPayment => oldPayment.member = member);

		let value = 0;
		items.forEach(item => {
			if(item.price > 0)
				value += item.price;
		});

		let paidValue = 0;
		paid.forEach(oldPayment => {
			paidValue += oldPayment.value;
		});


		const final = value - paidValue;

		if(final <= 0){
			
			return res.status(400).send({error: 'bill_already_paid', error_description: 'requested order was already paid'});
		}

		if(final < payment.value && payment.method !== 'DINHEIRO'){
			
			return res.status(400).send({error: 'invalid_payment', error_description: 'changes are not allowed for payments different than cash'});
		}

		const hash = bcrypt.hashSync(foundOrder.user + timestamp + foundOrder.orderId, process.env.SALT);

		payment.set({authorizationDate: timestamp, authorizationId: hash});

		payment.validate()
			.then(() => {
				Order.findOneAndUpdate(
					{
						orderId
					},
					{
						$push: {payments: {member, payment}}
					},
					{
						new: true
					},
					(pushError, newDocument) => {
						if (pushError){
							
							return res.status(400).send({error: 'failed_to_push', error_description: 'couldnt query the database'});
						}

						
						const result = calculateValues(newDocument);
						next();
						return res.status(200).send(result);

					});
			})
			.catch(error => {
				
				res.status(400).send({error: 'failed_to_validate', error_description: `invalid payment format: ${error.message}`});
			});

	});

});

router.post('/:orderId/checkout', (req, res, next) => {
	const {orderId} = req.params;

	Order.findOne({orderId}, (findError, foundOrder) => {
		if (findError){
			
			return res.status(500).send({error: 'internal_error', error_description: 'something went happened'});
		}

		if (foundOrder == null){
			
			return res.status(400).send({error: 'not_found', error_description: 'requested order was not found'});
		}else{
			if(foundOrder.closed == 'false') {
				let result = calculateValues(foundOrder);

				if(result.remaining > 0){
					
					return res.status(400).send({error: 'bill_still_open', error_description: 'the order needs to be paid before closing'});
				}

				result.closed = true;
				//@TODO: Socket.io callback

				
				next();
				return res.status(201).send(result);
			}else {
				
				return res.status(400).send({error: 'not_found', error_description: 'requested order was not found'});
			}}


	});
});


module.exports = router;
