const mongoose = require('mongoose');

const WaitingListSchema = new mongoose.Schema({
	costumerId: { type: String },
	name: { type: String, required: true },
}, {collection: 'waitinglist', timestamps: true});

const WaitingList = mongoose.model('WaitingList', WaitingListSchema);

module.exports = WaitingList;
