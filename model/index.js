const Product = require('./productModel');
const Order = require('./orderModel');
const Pizza = require('./pizzaModel');
const Flavor = require('./flavorModel');
const User = require('./userModel');
const Table = require('./tableModel');
const Payment = require('./paymentModel');
const Delivery = require('./deliveryModel');
const WaitingList = require('./waitingListModel');
const Client = require('./clientModel');
const Provider = require('./providerModel');
const Customer = require('./customerModel');

module.exports = {
	Product,
	Order,
	Pizza,
	Flavor,
	User,
	Table,
	Payment,
	Delivery,
	WaitingList,
	Client,
	Provider,
	Customer
};
