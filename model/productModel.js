const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
	code: { type: String, required: true, unique: true },
	ref: { type: String, required: true, unique: true },
	name: { type: String, required: true },  //FIXME: mudar para "title"
	description: { type: String },
	group: {type: String, default: '1'},
	subgroup: {type: String, default: '01'},
	variation: {type: String, default: '01'},
	unity: {type: String, enum:['UN', 'FD', 'KG', 'PCT', 'CX', 'LT'], default: 'UN'},
	provider: { type: String, required: true, default: 'LA SOLANA' },
	price: {type: Number, required: true}
}, { collection: 'products', timestamps: true });

const Product = mongoose.model('Product', ProductSchema);

module.exports = Product ;
