const mongoose = require('mongoose');

const FlavorSchema = new mongoose.Schema({
	code: {type: String, required: true, unique: true},
	name: { type: String, required: true },
	description: { type: [String], require: true },
	group: {type: String, required: true},
	variation: {type: String},
	provider: {type: String, default: 'LASOLANA'},
	price: {type: Number, required: true}
});

const Flavor = mongoose.model('Flavor', FlavorSchema);

module.exports = Flavor;
