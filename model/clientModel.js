const mongoose = require('mongoose');

const ClientSchema = new mongoose.Schema({
	clientId: { type: String, required: true, unique: true },
	clientSecret: { type: String, required: true },
	grantTypes: { type: [String], default: ['password'] },
	scopes: { type: [String], default: ['password'] },
}, { collection: 'clients' });

const Client = mongoose.model('Client', ClientSchema);

module.exports = Client;
