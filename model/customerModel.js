const mongoose = require('mongoose');

const cpfRegex = /([0-9]{2}[.]?[0-9]{3}[.]?[0-9]{3}[/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2})/;

const CustomerSchema = new mongoose.Schema({
	customerId: { type: String },
	name: { type: String, required: true },
	address: { type: String, required: true },
	cpf: {
		type: String,
		unique: true,
		required: true,
		validate: { validator: (item) => cpfRegex.test(item), message: 'invalid cpf' },
	},
	phoneNumber: { type: String, required: true },
	email: { type: String, required: true },
	historic: [{ type: String }],
	creationDate: Date,
});

const Customer = mongoose.model('Customer', CustomerSchema);

module.exports = Customer;
