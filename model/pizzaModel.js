const mongoose = require('mongoose');

const PizzaSchema = new mongoose.Schema({
	name: { type: [String], required: true, enum: ['PEQUENA SIMPLES', 'MÉDIA SIMPLES', 'GRANDE SIMPLES'] },
	code: { type: String, require: true },
	description: { type: String, require: true },
	flavorLimit: {type: Number},
});

const Pizza = mongoose.model('Pizza', PizzaSchema);

module.exports = Pizza;
