const User = require('../../model').User;
const Client = require('../../model').Client;
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const bcrypt = require('bcrypt');

const server = new MongoMemoryServer();


const populateDatabase = () => new Promise((resolve, reject) => {	
	try{
		server.getConnectionString()
			.then(url => {
				mongoose.connect(url)
					.then(db => {
						const user = new User({
							name: 'Administrator',
							email: 'admin@admin.com',
							password: bcrypt.hashSync('123456', process.env.SALT),
						});
				
						const client = new Client({
							clientId: 'lasolana',
							clientSecret: 'minhamarguerita'
						});
				
						client.save()
							.then(result => {
								console.log(`Client "${result.clientId}" pushed to DB => "${result._id}"`);
								user.save()
									.then(result => {
										console.log(`Administrator pushed to DB => "${result._id}"`);
										resolve();
									})
									.catch(error => {
										console.log(`Init Administrator failed => "${error.message}"`);
										reject(error);
									});
							})
							.catch(error => {
								console.log(`Init client failed => "${error.message}"`);
								reject(error);
							});
	
						server.getDbName()
							.then(name => db.connection.useDb(name));
					})
					.catch(error => {throw new Error(error);});
			});
	}catch(error) {
		console.log(error);
		reject(error);
	}
});

const close = async http => {
	if (http) {
		await http.close();
	}
	mongoose.connections.forEach(async con => {
		await con.close();
	});
	server.stop();
};

module.exports = {
	populateDatabase,
	close
};