
const request = require('supertest');
const app = require('../app');
const faker = require('faker');
const {assert} = require('chai');

let JWT;

const EMAIL = faker.internet.email();
const NAME = faker.name.firstName();
const ADDRESS = faker.address.streetAddress();
const CPF = '08485806999';
const PHONE = faker.phone.phoneNumber();


describe('Create CUSTOMER with invalid data', function() {

	before(async function() {
		const token = await request(app)
			.post('/oauth/token')
			.set('Content-Type', 'application/x-www-form-urlencoded')
			.send(encodeURI('username=admin@admin.com&password=123456&grant_type=password'))
			.auth('lasolana', 'minhamarguerita', { type: 'basic' });
		JWT = token.body.access_token;
	});
  
	it('should not create a new customer without content-type', async function () {
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'plain/text')
			.set('Authorization', 'Bearer ' + JWT)
			.send(
				'name: NAME,' +
				'phoneNumber: PHONE,' +
				'cpf: CPF,' +
				'address: ADDRESS,' +
				'email: EMAIL'
			);

		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'invalid_content',  'Expect error code to be \"invalid_content\"');

	});

	it('should not create a new customer without a token', async function () {
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ')
			.send({
				name: NAME,
				phoneNumber: PHONE,
				cpf: CPF,
				address: ADDRESS,
				email: EMAIL
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'invalid_token',  'Expect server to require a token');

	});

	it('should not create a new customer without name', async function () {

		

		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: '',
				phoneNumber: PHONE,
				cpf: CPF,
				address: ADDRESS,
				email: EMAIL
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'failed_to_validate',  'Expect error code to be \"failed_to_validate\"');

	});

	it('should not create a new customer without email', async function () {

		
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: NAME,
				phoneNumber: PHONE,
				cpf: CPF,
				address: ADDRESS,
				email: ''
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'failed_to_validate',  'Expect error code to be \"failed_to_validate\"');

	});

	it('should not create a new customer without phone', async function () {

		
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: NAME,
				phoneNumber: '',
				cpf: CPF,
				address: ADDRESS,
				email: EMAIL
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'failed_to_validate',  'Expect error code to be \"failed_to_validate\"');

	});


	it('should not create a new customer without cpf', async function () {

		
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: NAME,
				phoneNumber: PHONE,
				cpf: '',
				address: ADDRESS,
				email: EMAIL
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'failed_to_validate',  'Expect error code to be \"failed_to_validate\"');

	});


	it('should not create a new customer without address', async function () {

		
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: NAME,
				phoneNumber: PHONE,
				cpf: CPF,
				address: '',
				email: EMAIL
			});
		assert.equal(res.statusCode, 400, 'Expect request to be denied (400)');
		assert.equal(res.body.error, 'failed_to_validate',  'Expect error code to be \"failed_to_validate\"');

	});
  
	it('should create a new customer', async function () {
		
		const res = await request(app)
			.post('/api/v1/customer')
			.set('Content-Type', 'application/json')
			.set('Authorization', 'Bearer ' + JWT)
			.send({
				name: NAME,
				phoneNumber: PHONE,
				cpf: CPF,
				address: ADDRESS,
				email: EMAIL
			});
					
		assert.equal(res.statusCode, 201, 'Expect user to be created (201)');
		assert.equal(res.body.email, EMAIL, 'Expect user\'s email to match');
		assert.exists(res.body._id, 'Expect user to have an ID');

	});
	
});


